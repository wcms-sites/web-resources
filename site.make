core = 7.x
api = 2

; Location content type
projects[uw_ct_location][type] = "module"
projects[uw_ct_location][download][type] = "git"
projects[uw_ct_location][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_location.git"
projects[uw_ct_location][download][tag] = "7.x-1.5"
projects[uw_ct_location][subdir] = ""

; Writer Tips content type
projects[uw_ct_writer_tips][type] = "module"
projects[uw_ct_writer_tips][download][type] = "git"
projects[uw_ct_writer_tips][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_writer_tips.git"
projects[uw_ct_writer_tips][download][tag] = "7.x-1.6"
projects[uw_ct_writer_tips][subdir] = ""

; UWaterloo Web Resources Customizations
projects[uw_web_resources_customizations][type] = "module"
projects[uw_web_resources_customizations][download][type] = "git"
projects[uw_web_resources_customizations][download][url] = "https://git.uwaterloo.ca/wcms/uw_web_resources_customizations.git"
projects[uw_web_resources_customizations][download][tag] = "7.x-1.0"
projects[uw_web_resources_customizations][subdir] = ""
